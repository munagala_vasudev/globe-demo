package com.hcl.globe.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class LoadProperties {
	public static String loadProperty() throws IOException {
		final Logger logger = Logger.getLogger(LoadProperties.class);
		String ipaddress;
		logger.info("loading the properties");
		Properties prop = new Properties();
		InputStream input = AddToCart.class.getClassLoader().getResourceAsStream("config.properties");
		prop.load(input);
		ipaddress = prop.getProperty("ipaddress");
		logger.info("ipaddress: "+ipaddress);
		logger.info("ipaddress is returned");
		return ipaddress;
	}
}
