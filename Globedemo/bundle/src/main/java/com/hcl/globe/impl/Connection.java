package com.hcl.globe.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;


public class Connection{
	//static String authentication;
	final static Logger logger = Logger.getLogger(Connection.class);
	
	public static HttpURLConnection connection(URL url, String requestType, String authentication)throws IOException, JSONException
	{ 
		logger.info("calling connection..");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(requestType.toUpperCase());
		conn.setRequestProperty("Authorization", authentication);
		conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		
		return conn;
	}
	
	public static JSONObject authentication(URL url, String authentication) throws IOException, JSONException{

		HttpURLConnection conn = null;
		JSONObject obj = null;
		String output=null;

		conn=connection(url, "GET",authentication);
		
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		while ((output = br.readLine()) != null)
		{
			obj = new JSONObject(output);
		}
		return obj;
	}


}
