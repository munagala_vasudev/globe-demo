package com.hcl.globe.impl;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("serial")
@SlingServlet(paths="/bin/slots", methods = "GET", metatype=true)
public class Slots extends SlingAllMethodsServlet
{
	final Logger logger = Logger.getLogger(Slots.class);
	public static URL url;
	public static String initialUrl;
	static String AuthenticationValue;
	static HttpURLConnection conn;
	static BufferedReader br;
	static String output;
	JSONObject eachOption;
	JSONArray optionsArray = new JSONArray();
	String ipaddress;
	static String authentication;

	ArrayList<String> href =new ArrayList<String>();
	

	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException
	{
		final Logger logger = Logger.getLogger(Slots.class);
		ipaddress = LoadProperties.loadProperty();
		URL url;
		JSONObject obj;
		try
		{
			authentication = TokenGeneration.generateToken(ipaddress);
			url = new URL("http://"+ipaddress+":9080/cortex/slots/nortonstoreplus");
			obj = Connection.authentication(url, authentication);
			final JSONArray geodata = obj.getJSONArray("links");
			final int n = geodata.length();
			for (int i = 0; i < n; ++i) {
				final JSONObject person = geodata.getJSONObject(i);
				href.add(person.getString("href"));
			}
			logger.info("Hi22" +href.size());
			for(int j=0; j<href.size();j++)
			{
				url = new URL(href.get(j));
				obj = Connection.authentication(url, authentication);
				eachOption = new JSONObject();
				eachOption.put("text", obj.get("name").toString());
				eachOption.put("value", obj.get("name").toString());
				optionsArray.put(eachOption);
			}
			
			JSONObject finalJsonResponse = new JSONObject();
			//Adding this finalJsonResponse object to showcase optionsRoot property functionality
			finalJsonResponse.put("root", optionsArray);
			logger.info("Hi22++++++++++" +optionsArray);
			resp.getWriter().println(finalJsonResponse.toString());
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	
}
