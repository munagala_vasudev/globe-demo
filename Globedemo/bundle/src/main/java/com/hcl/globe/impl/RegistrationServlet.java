package com.hcl.globe.impl;

import java.awt.print.Printable;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


@SlingServlet(paths="/bin/mobee/newaccount", methods = "GET", metatype=true)


public class RegistrationServlet extends SlingAllMethodsServlet
{
	final static Logger logger = Logger.getLogger(RegistrationServlet.class); 
private static String authentication;
private String ipaddress;
	


	@Override
	protected void doGet(final SlingHttpServletRequest req,
			final SlingHttpServletResponse resp) throws ServletException, IOException{
		URL url;
		ipaddress = LoadProperties.loadProperty();
		logger.info(ipaddress);
		 try{
			authentication = TokenGeneration.generateToken(ipaddress);
		 }
		 catch(JSONException je)
		 {
			 je.printStackTrace();
		 }
		
		 logger.info("token value:-"+authentication);
		String emailFormUrl = "http://"+ipaddress+":9080/cortex/registrations/nortonstoreplus/newaccount/form"; 


		
		String fname=req.getParameter("fname");
		String lname=req.getParameter("lname");
		String password=req.getParameter("password");
		String email=req.getParameter("email");
		logger.info("Message : " +fname);
		logger.info("Message : " +lname);
		logger.info("Message : " +password);
		logger.info("Message : " +email);
		
		String userDetails = "{\"family-name\":\""+fname+"\",\"given-name\":\""+lname+"\",\"password\":\""+password+"\",\"username\":\""+email+"\"}";

		JSONObject userDetailsJson = null;

		try {
			userDetailsJson = new JSONObject(userDetails);
			url=new URL(emailFormUrl);
			HttpURLConnection conn = Connection.connection( url, "GET", authentication);
			logger.info("HttpURLConnection code : " +conn.getInputStream());
			String status = populateWithUIData(conn, userDetailsJson);
			logger.info("status code : " +status);
			resp.getWriter().print(status);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		resp.setContentType("text/plain");


	}
	private static String populateWithUIData(HttpURLConnection conn, JSONObject infoJson) throws IOException, JSONException {
		String output;
		JSONObject obj = null;
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		
		while ((output = br.readLine()) != null) {
		
			obj = new JSONObject(output);
			
		}
		
		JSONObject requestJson = new JSONObject();
		
		String[] objArray = JSONObject.getNames(obj);
		for (int i = 0; i < objArray.length; i++) {
			if (infoJson.has(objArray[i]))
				requestJson.put(objArray[i], infoJson.get(objArray[i]));
		}

		JSONArray linksArray = (JSONArray) obj.get("links");
		String redirectUrl = "";
		for (int i = 0; i < linksArray.length(); i++) {
			JSONObject link = (JSONObject) linksArray.get(i);
			if (link.has("href"))
				redirectUrl = link.getString("href");
		}
		URL url=new URL(redirectUrl);
		String statuscode = postDataToEP(requestJson, url, "POST");
	
		return statuscode;

	}

	

	public static String postDataToEP(JSONObject requestJson, URL url, String requestType)throws IOException, JSONException
	{
		HttpURLConnection conn = Connection.connection(url, requestType,authentication);
		if ("POST".equalsIgnoreCase(requestType))
		{
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.write(requestJson.toString().getBytes());
			wr.flush();
			wr.close();
		}
		String responsecode = ""+conn.getResponseCode();
		logger.info("Status Code:: " + responsecode);
		
		return responsecode;
		 
	}



	
}