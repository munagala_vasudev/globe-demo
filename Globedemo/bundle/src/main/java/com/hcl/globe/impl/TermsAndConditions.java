package com.hcl.globe.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/termsAndConditions", methods = "GET", metatype=true)
public class TermsAndConditions extends SlingAllMethodsServlet
{
	final Logger logger = Logger.getLogger(TermsAndConditions.class);
	private String authentication;
	private String ipaddress;
	
	@Override
	protected void doGet(final SlingHttpServletRequest req,final SlingHttpServletResponse resp) throws ServletException, IOException 
	{
		logger.info("Get Method started");
		ipaddress = LoadProperties.loadProperty();
		URL url;
		try{
			authentication = TokenGeneration.generateToken(ipaddress);
			
			url = new URL("http://"+ipaddress+":9080/cortex/terms/id=");
			logger.info("Terms&conditions url: "+url);
			JSONObject jsonObj = Connection.authentication(url, authentication);
			String data = jsonObj.get("message").toString();
			logger.info(data);
			resp.getWriter().print(data);
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
}
