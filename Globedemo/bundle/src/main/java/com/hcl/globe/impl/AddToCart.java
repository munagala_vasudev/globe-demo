package com.hcl.globe.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/addtocart", methods = "GET", metatype=true)
public class AddToCart extends SlingAllMethodsServlet
{
	final Logger logger = Logger.getLogger(AddToCart.class);
	private static String authentication;
	private String ipaddress;

	@Override
	protected void doGet(final SlingHttpServletRequest req,final SlingHttpServletResponse resp) throws ServletException, IOException 
	{
		logger.info("Get Method started");
		URL url;
		Map<String, String> item_links = new HashMap<String, String>();
		Map<String, String> cart_links = new HashMap<String, String>();
		JSONObject obj;
		
		ipaddress = LoadProperties.loadProperty();
		logger.info(ipaddress);
		
		String quantity = req.getParameter("quantity");
		int qty = Integer.parseInt(quantity);
		logger.info("Quantity: "+qty);
		try{
			authentication = TokenGeneration.generateToken(ipaddress);
			logger.info("authentication code generated" +authentication);
			String searchFormUrl = "http://"+ipaddress+":9080/cortex/searches/nortonstoreplus/keywords/form";
			String userDetails = "{\"keywords\":\"Norton Security Plus\",\"page-size\":\"5\"}";
			JSONObject userDetailsJson = new JSONObject(userDetails);
			 url=new URL(searchFormUrl);
			HttpURLConnection conn = Connection.connection( url, "GET", authentication);
			String redirectLocation = populateWithUIData(conn, userDetailsJson);

			url = new URL(redirectLocation);
			obj = Connection.authentication(url,authentication);
			final JSONArray geodata = obj.getJSONArray("links");
			final int n = geodata.length();
			if(n==0){
				logger.info("No Product Found with the Search Keyword..");
				resp.getWriter().print("No Product Found with the Search Keyword..");
			}
			else
			{
				JSONObject person = null;
				for (int i = 0; i < n; ++i)
				{
					person = geodata.getJSONObject(i);
				}
				url = new URL(person.getString("href"));
				obj =Connection.authentication(url,authentication);
				final JSONArray geodata1 = obj.getJSONArray("links");
				final int n1 = geodata1.length();
				for (int i = 0; i < n1; ++i)
				{
					final JSONObject links = geodata1.getJSONObject(i);
					links.get("rel").toString();
					links.get("href").toString();
					item_links.put(links.get("rel").toString(), links.get("href").toString());
				}
			}
			String availableurl = item_links.get("availability");
			String status = null;
			if (availableurl != null){
				url = new URL(availableurl);
				obj = Connection.authentication(url,authentication);
				status = obj.get("state").toString();
			}
			if("AVAILABLE".equals(status)){
				String addtocartformurl = item_links.get("addtocartform");
				
				if (addtocartformurl != null)
				{
					String quantityDetails = "{\"quantity\":\""+qty+"\"}";
					JSONObject userDetailsJson1 = new JSONObject(quantityDetails);
					url=new URL(addtocartformurl);
					HttpURLConnection conn2 = Connection.connection(url, "GET",authentication);
					String redirectLocation2 = populateWithUIData(conn2, userDetailsJson1);//produce is added to cart
					url = new URL(redirectLocation2);
					obj = Connection.authentication(url,authentication);
					final JSONArray geodata1 = obj.getJSONArray("links");
					final int n1 = geodata1.length();
					for (int i = 0; i < n1; ++i)
					{
						final JSONObject links = geodata1.getJSONObject(i);
						links.get("rel").toString();
						links.get("href").toString();
						cart_links.put(links.get("rel").toString(), links.get("href").toString());
						logger.info("cart Links: "+cart_links);
					}
				}
				String totalurl = cart_links.get("total");
				if (totalurl != null)
				{
					url = new URL(totalurl);
					obj = Connection.authentication(url,authentication);
					final JSONArray geodata1 = obj.getJSONArray("cost");
					final int n1 = geodata1.length();
					for (int i = 0; i < n1; ++i)
					{
						final JSONObject links = geodata1.getJSONObject(i);
						logger.info("final price:-"+links.get("display").toString());
						resp.getWriter().print(links.get("display").toString());
					}
				}
				else
				{
					logger.info("stock not available");
					resp.getWriter().print(" Out of Stock ");
				}
			}
			else
			{
				logger.info("availableurl is null");
				resp.getWriter().print(" availableurl is null ");
			}
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private static String populateWithUIData(HttpURLConnection conn, JSONObject infoJson) throws IOException, JSONException {
		String output;
		JSONObject obj = null;
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		while ((output = br.readLine()) != null)
		{
			obj = new JSONObject(output);
		}

		JSONObject requestJson = new JSONObject();
		String[] objArray = JSONObject.getNames(obj);
		for (int i = 0; i < objArray.length; i++) {
			if (infoJson.has(objArray[i]))
				requestJson.put(objArray[i], infoJson.get(objArray[i]));
		}

		JSONArray linksArray = (JSONArray) obj.get("links");
		String redirectUrl = null ;
		for (int i = 0; i < linksArray.length(); i++)
		{
			JSONObject link = (JSONObject) linksArray.get(i);
			if (link.has("href"))
				redirectUrl = link.getString("href");
		}
		URL url=new URL(redirectUrl);
		String location = postDataToEP(requestJson, url, "POST");
		return location;
	}

	
	public static String postDataToEP(JSONObject requestJson, URL url, String requestType)throws IOException, JSONException
	{
		HttpURLConnection conn = Connection.connection(url, requestType,authentication);
		if ("POST".equalsIgnoreCase(requestType))
		{
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.write(requestJson.toString().getBytes());
			wr.flush();
			wr.close();
		}
		conn.setInstanceFollowRedirects(false);
		return conn.getHeaderField("Location");
	}
	
}