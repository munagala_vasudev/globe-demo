<%@ include file="/libs/foundation/global.jsp" %>
<html>
    <head>
      <style>
	.greenBannerPlatinumButton {

    appearance: button;
    border: 2px solid #fff;

    text-decoration: none;
    color: initial;
}
      </style>
    </head>
	<body>
	<div class="mainBodyTrenta" id="aui_3_2_0_1117">
    	<div class="fluid-width linear z8 shadow career" id="aui_3_2_0_1116">
    	    <div class="fixed-width center">
        	    <p class="textBox"></p>
            	<div class="rightAlign textBoxCareer">
                	<p class="quote"></p>
                	<div class="greenland">
                        <h1 style="color: #fff;">${properties.caption}</h1>
                        <p style="color: #fff; font-size:150%;" >${properties.para}</p>
    	                <p>&nbsp;</p>
        	            <p>&nbsp;</p>
            	     <p>  <a style="color: #fff; font-size:150%;" class="greenBannerPlatinumButton" href="http://www.globe.com.ph/about-globe/corporate-info">View Corporate Information</a>
                        </p>
	                </div>
    	            <p></p>
        	        <p class="name"></p>
            	    <p class="position"></p>
           	 	</div>
        	</div>
        	<div class="imgHolderCareer" id="aui_3_2_0_1115">
        	    <img src="${properties.fileReference}" id="aui_3_2_0_1114">
        	</div>
        	<div class="imgHolderCareerMob">
        	    <img src="/image/image_gallery?uuid=13082c2d-5e7e-4952-a36d-fad94e85e1cc&amp;groupId=7122541&amp;t=1469110962021">
        	</div>
    	</div>
	</div>
	</body>
</html>