<%@ include file="/libs/foundation/global.jsp" %>

<div class="fixedHeaderContainer" >
    <div class="hideOnMobile upperHeaderContainer fixed-width center">
<div class="upperHeaderRightPart"> 

<ul class="upperRightNav">
		<%
        String title[] = properties.get("title", new String[0]);
        String links[] = properties.get("path", new String[0]);
        
        for(int i=0; i<title.length ;i++)
        {  %> 

        <li><a href="<%=links[i]%>"><%=title[i]%></a></li> 
      <% }

	%>

</ul>
</div>

        </div>
    </div><br><br>