<%@ include file="/libs/foundation/global.jsp" %>
<%String title = properties.get("para", "");%>
<div class="carousel-bg shadow z15">
	<div class="lavaCarousel fluid-width pull-left carouselHeight" id="fold1">
		<div class="fixed-width center carousel-main-content">
			<div class="carousel carousel-home one slide" id="myCarousel">
				<div class="carousel-holder carousel-inner carousel-inner-home">
					<div class="item active">
						<div class="hero-media hero-media-home">
                            <img src="${properties.file1}" alt="carousel_image">
						</div>
						<div class="carousel-text">
							<h2><%= title%> </h2>
							<p> </p>
							<div style="font-size:15px;">Create your story with Samsung Galaxy Note7 and myLifestyle Plan.</div><br>
							<p></p>
                            <div class="social-sharing">

                                    <div align="right" style="float:right;">
                                        <button style="font-size:31px; !important" type="button" onclick="location.href='<%=properties.get("RegistrationButton")%>'+'.html';">${properties.textonbutton}</button>

								</div>
							</div>
						</div>
					</div>
                </div>

			</div>
		</div>
	</div>
</div>
